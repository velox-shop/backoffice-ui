import {Profile} from "oidc-client";

export interface Token {
  userInfo: Profile | null | undefined,
  idToken: string | null | undefined,
  accessToken: string | null | undefined,
  authenticated: boolean,
  isAdminCart: boolean,
  isAdminCurrency: boolean,
  isAdminPrice: boolean,
  isAdminAvailability: boolean,
  isAdminProduct: boolean,
  isAdminUser: boolean,
  isAdminOrder: boolean
}
