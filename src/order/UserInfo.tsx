import React from "react";
import {Row, Col} from "react-bootstrap";
import {useTranslation} from 'react-i18next';
import {UserInfoProps} from "../cart/UserInfo";

const UserInfo = ({user} : UserInfoProps) => {
  const {t} = useTranslation();

  return (
      <Col className="values">
        <Row>
          <Col> <>{t(
              'order.userInfo.userInfo.id.value')}: {user.id}</> </Col>
          <Col> <>{t(
              'order.userInfo.userInfo.firstName.value')}: {user.firstName}</> </Col>
          <Col> <>{t(
              'order.userInfo.userInfo.lastName.value')}: {user.lastName}</> </Col>
          <Col> <>{t(
              'order.userInfo.userInfo.email.value')}: {user.email}</> </Col>
          <Col> <>{t(
              'order.userInfo.userInfo.phone.value')}: {user.phone}</> </Col>
        </Row>
      </Col>
  );

};

export default UserInfo;