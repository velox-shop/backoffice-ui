import {combineReducers} from 'redux';

import language from "./language";
import addAuthData from "./addAuthData";

export default combineReducers({
  addAuthData,
  language,
});