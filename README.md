# Backoffice UI

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).


|Version      | Description                                                                                                                                          |
|-------------|------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1.6.0       | Introduced TypeScript (all components have been translated from JS to TS)                                                                            |
| 1.5.0       | Introduced usage of semantic versioning and conventional commits                                                                                     |
| 1.4.0       | Use availability status as a translation key                                                                                                         |
| 1.3.0       | Create model from swagger file (Typescript)                                                                                                          |
| 1.2.1       | Backoffice icons alignment                                                                                                                           |
| 1.2.0       | Backoffice cart                                                                                                                                      |
| 1.1.1       | Backoffice Localization                                                                                                                              |
| 1.0.1       | Fix Wrong Error message on login                                                                                                                     |

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3001](http://localhost:3001) to view it in the browser.

#### Access the service
Backoffice main service: [http://localhost:3001/backoffice](http://localhost:3001/backoffice).

Backoffice cart list: [http://localhost:3001/backoffice/cart](http://localhost:3001/backoffice/cart).

Backoffice price list: [http://localhost:3001/backoffice/price](http://localhost:3001/backoffice/price).

Backoffice availability list: [http://localhost:3001/backoffice/availability](http://localhost:3001/backoffice/availability).

Backoffice order list: [http://localhost:3001/backoffice/order](http://localhost:3001/backoffice/order).

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify

## Environment variables

The environment variables should be added to the .env file and the docker-compose.yml file.

The env.sh script reads value of variable if exists as Environment variable in docker-compose.yml file, otherwise it use value from .env file.

The created env variables are stored in the env-config.js file.

When run locally npm start executes the env.sh script and creates the env-config.js file. When run with the docker, both env.sh and .env file are copied in the image and env.sh script is executed when
the nginx in the container is started.    

## Running in Docker

### Production build test run
To test App production build in simple Nginx container invoke:
```
docker build -t velox-backoffice-ui . && docker run --rm --name velox-backoffice-ui -p 3001:3001 velox-backoffice-ui
```
This will create and start Nginx container mapped to local port 3001 serving optimized production build.

### Development inside Docker
To run app in dynamic development mode (as with `npm start`) inside Docker invoke:
```
docker run -it --rm -v `pwd`:/app -p 3001:3001 node /bin/bash -c "cd /app && npm install && npm start"
```
This will create Node container mapped to local port 3001 running App in development mode.
