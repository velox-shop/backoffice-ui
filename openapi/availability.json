{
  "openapi": "3.0.1",
  "info": {
    "title": "Availability API",
    "description": "This service provides information on availability of the articles"
  },
  "servers": [
    {
      "url": "http://docker:8445/availability/v1",
      "description": "Generated server url"
    }
  ],
  "tags": [
    {
      "name": "Availability",
      "description": "the Availability API"
    }
  ],
  "paths": {
    "/availabilities": {
      "get": {
        "tags": [
          "Availability"
        ],
        "summary": "Get Availabilities",
        "operationId": "getAvailabilities",
        "parameters": [
          {
            "name": "pageable",
            "in": "query",
            "description": "Pagination will be ignored when articleIds are provided",
            "required": true,
            "schema": {
              "$ref": "#/components/schemas/Pageable"
            }
          },
          {
            "name": "articleId",
            "in": "query",
            "description": "Filter by Id of the Article. Return all availabilities matching of the given articleIds",
            "required": false,
            "schema": {
              "type": "array",
              "items": {
                "type": "string"
              }
            }
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation",
            "content": {
              "*/*": {
                "schema": {
                  "$ref": "#/components/schemas/AvailabilityDto"
                }
              }
            }
          }
        }
      },
      "post": {
        "tags": [
          "Availability"
        ],
        "summary": "creates Availability",
        "operationId": "createAvailability",
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/AvailabilityDto"
              }
            }
          },
          "required": true
        },
        "responses": {
          "201": {
            "description": "returns the created availability",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/AvailabilityDto"
                }
              }
            }
          },
          "409": {
            "description": "availability already exists"
          },
          "422": {
            "description": "Mandatory parameters are missing. (e.g. Article Id)"
          }
        }
      }
    },
    "/availabilities/{id}": {
      "get": {
        "tags": [
          "Availability"
        ],
        "summary": "Get Availability by id",
        "operationId": "getAvailability",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "Id of the Availability. Cannot be empty.",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation",
            "content": {
              "*/*": {
                "schema": {
                  "$ref": "#/components/schemas/AvailabilityDto"
                }
              }
            }
          },
          "404": {
            "description": "availability not found",
            "content": {
              "*/*": {
                "schema": {
                  "$ref": "#/components/schemas/AvailabilityDto"
                }
              }
            }
          },
          "422": {
            "description": "Mandatory parameters are missing. (e.g. Article Id)"
          }
        }
      },
      "delete": {
        "tags": [
          "Availability"
        ],
        "summary": "Delete Availability by id",
        "operationId": "deleteAvailability",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "Id of the Availability. Cannot be empty.",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "204": {
            "description": "successful operation"
          },
          "404": {
            "description": "availability not found"
          }
        }
      },
      "patch": {
        "tags": [
          "Availability"
        ],
        "summary": "Updates an Availability",
        "operationId": "updateAvailability",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "Id of the Availability. Cannot be empty.",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "requestBody": {
          "description": "Availability to update. Cannot be empty.",
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/AvailabilityDto"
              }
            }
          },
          "required": true
        },
        "responses": {
          "200": {
            "description": "successful operation",
            "content": {
              "*/*": {
                "schema": {
                  "$ref": "#/components/schemas/AvailabilityDto"
                }
              }
            }
          },
          "404": {
            "description": "availability not found"
          },
          "409": {
            "description": "availability id cannot be changed"
          },
          "422": {
            "description": "Mandatory data missing"
          }
        }
      }
    }
  },
  "components": {
    "schemas": {
      "Pageable": {
        "type": "object",
        "properties": {
          "page": {
            "minimum": 0,
            "type": "integer",
            "format": "int32"
          },
          "size": {
            "maximum": 2000,
            "minimum": 1,
            "type": "integer",
            "format": "int32"
          },
          "sort": {
            "type": "array",
            "items": {
              "type": "string"
            }
          }
        }
      },
      "AvailabilityDto": {
        "required": [
          "articleId"
        ],
        "type": "object",
        "properties": {
          "id": {
            "type": "string",
            "description": "Unique identifier of the Availability.",
            "example": "5dc618af-af49-4adc-bccd-4d17aeff7526"
          },
          "articleId": {
            "maxLength": 50,
            "minLength": 3,
            "type": "string",
            "description": "Unique identifier of the article.",
            "example": "123"
          },
          "quantity": {
            "type": "number",
            "description": "Non-negative available quantity of the article.",
            "example": 10
          },
          "status": {
            "type": "string",
            "description": "Availability stock status of the article.",
            "example": "IN_STOCK",
            "enum": [
              "ALWAYS_AVAILABLE",
              "NOT_AVAILABLE",
              "IN_STOCK"
            ]
          },
          "replenishmentTime": {
            "type": "integer",
            "description": "Standard lead time in days for article quantity replenishment.",
            "format": "int32",
            "example": 14,
            "default": 0
          }
        }
      }
    }
  }
}