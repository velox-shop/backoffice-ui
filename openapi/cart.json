{
  "openapi": "3.0.1",
  "info": {
    "title": "Cart API",
    "description": "This service provides CRUD functionality for Carts."
  },
  "servers": [
    {
      "url": "http://docker:8446/cart/v1",
      "description": "Generated server url"
    }
  ],
  "tags": [
    {
      "name": "Cart",
      "description": "the Cart API"
    },
    {
      "name": "Item",
      "description": "the Item API"
    }
  ],
  "paths": {
    "/carts/{id}": {
      "get": {
        "tags": [
          "Cart"
        ],
        "summary": "Find Cart by code",
        "operationId": "getCart",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "Id of the Cart. Cannot be empty.",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/CartDto"
                }
              }
            }
          },
          "404": {
            "description": "cart not found"
          }
        }
      },
      "delete": {
        "tags": [
          "Cart"
        ],
        "summary": "Delete Cart by code",
        "operationId": "removeCart",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "Id of the Cart. Cannot be empty.",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "204": {
            "description": "successful operation"
          },
          "404": {
            "description": "cart not found"
          }
        }
      },
      "patch": {
        "tags": [
          "Cart"
        ],
        "summary": "Update a cart. Update currency and assign anonymous cart by code to current user",
        "operationId": "updateCart",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "Id of the Cart. Cannot be empty.",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "requestBody": {
          "description": "The cart to update.",
          "content": {
            "application/json": {
              "schema": {
                "type": "object",
                "properties": {
                  "cart": {
                    "$ref": "#/components/schemas/CartDto"
                  },
                  "userId": {
                    "type": "string"
                  }
                }
              }
            }
          },
          "required": true
        },
        "responses": {
          "200": {
            "description": "successful operation",
            "content": {
              "*/*": {
                "schema": {
                  "$ref": "#/components/schemas/CartDto"
                }
              }
            }
          },
          "403": {
            "description": "non anonymous cart id provided",
            "content": {
              "*/*": {
                "schema": {
                  "$ref": "#/components/schemas/CartDto"
                }
              }
            }
          },
          "404": {
            "description": "cart not found",
            "content": {
              "*/*": {
                "schema": {
                  "$ref": "#/components/schemas/CartDto"
                }
              }
            }
          }
        }
      }
    },
    "/carts": {
      "get": {
        "tags": [
          "Cart"
        ],
        "summary": "gets all carts. Paginated.",
        "operationId": "getCarts",
        "parameters": [
          {
            "name": "pageable",
            "in": "query",
            "required": true,
            "schema": {
              "$ref": "#/components/schemas/Pageable"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/CartDto"
                }
              }
            }
          }
        }
      },
      "post": {
        "tags": [
          "Cart"
        ],
        "summary": "create new Cart",
        "operationId": "createCart",
        "requestBody": {
          "description": "The cart to create.",
          "content": {
            "application/json": {
              "schema": {
                "type": "object",
                "properties": {
                  "cartDto": {
                    "$ref": "#/components/schemas/CartDto"
                  },
                  "currentUser": {
                    "type": "string"
                  }
                }
              }
            }
          },
          "required": true
        },
        "responses": {
          "201": {
            "description": "Cart created",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/CartDto"
                }
              }
            }
          }
        }
      }
    },
    "/carts/{id}/items/{itemId}": {
      "get": {
        "tags": [
          "Item"
        ],
        "summary": "Gets an item by cartId and itemId",
        "operationId": "getItem",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "Id of the Cart. Cannot be empty.",
            "required": true,
            "schema": {
              "type": "string"
            }
          },
          {
            "name": "itemId",
            "in": "path",
            "description": "Id of the item. Cannot be empty.",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation",
            "content": {
              "*/*": {
                "schema": {
                  "$ref": "#/components/schemas/ItemDto"
                }
              }
            }
          },
          "404": {
            "description": "item not found"
          }
        }
      },
      "delete": {
        "tags": [
          "Item"
        ],
        "summary": "Removes an item from the cart",
        "operationId": "removeItem",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "Id of the Cart. Cannot be empty.",
            "required": true,
            "schema": {
              "type": "string"
            }
          },
          {
            "name": "itemId",
            "in": "path",
            "description": "Id of the item. Cannot be empty.",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation",
            "content": {
              "*/*": {
                "schema": {
                  "$ref": "#/components/schemas/CartDto"
                }
              }
            }
          },
          "404": {
            "description": "item not found"
          }
        }
      }
    },
    "/carts/{id}/items": {
      "post": {
        "tags": [
          "Item"
        ],
        "summary": "Adds an item to the cart",
        "operationId": "addItem",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "Id of the Cart. Cannot be empty.",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "requestBody": {
          "description": "Item to create. Cannot be empty.",
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/ItemDto"
              }
            }
          },
          "required": true
        },
        "responses": {
          "200": {
            "description": "Item was already existing, quantity was updated",
            "content": {
              "*/*": {
                "schema": {
                  "$ref": "#/components/schemas/CartDto"
                }
              }
            }
          },
          "201": {
            "description": "Item Created",
            "content": {
              "*/*": {
                "schema": {
                  "$ref": "#/components/schemas/CartDto"
                }
              }
            }
          },
          "404": {
            "description": "cart not found"
          },
          "409": {
            "description": "an item with given articleId already exists"
          }
        }
      },
      "patch": {
        "tags": [
          "Item"
        ],
        "summary": "Updates an item in the cart",
        "operationId": "updateItem",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "Id of the Cart. Cannot be empty.",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "requestBody": {
          "description": "Item to update. Cannot be empty.",
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/ItemDto"
              }
            }
          },
          "required": true
        },
        "responses": {
          "200": {
            "description": "successful operation",
            "content": {
              "*/*": {
                "schema": {
                  "$ref": "#/components/schemas/CartDto"
                }
              }
            }
          },
          "404": {
            "description": "item not found"
          },
          "422": {
            "description": "Mandatory data missing"
          }
        }
      }
    }
  },
  "components": {
    "schemas": {
      "CartDto": {
        "required": [
          "currencyId"
        ],
        "type": "object",
        "properties": {
          "id": {
            "type": "string",
            "description": "Unique identifier of the Cart.",
            "example": "4b7e6hjA-107b-476b-816a-51hh256a30d1"
          },
          "userId": {
            "type": "string",
            "description": "Unique identifier of the User.",
            "example": "4b7e6hjA-107b-476b-816a-51hh256a30d1"
          },
          "currencyId": {
            "type": "string",
            "description": "The ID of the currency of the cart.",
            "example": "0d449436-2702-4051-8a85-e3ec5b645959"
          },
          "items": {
            "type": "array",
            "description": "List of the Items in the Cart.",
            "items": {
              "$ref": "#/components/schemas/ItemDto"
            }
          }
        }
      },
      "ItemDto": {
        "required": [
          "articleId",
          "quantity"
        ],
        "type": "object",
        "properties": {
          "id": {
            "type": "string",
            "description": "Unique identifier of the Item.",
            "example": "2e5c9ba8-956e-476b-816a-49ee128a40c9"
          },
          "articleId": {
            "type": "string",
            "description": "Unique identifier of the Article.",
            "example": "article1"
          },
          "quantity": {
            "type": "number",
            "description": "Quantity of article in this Item.",
            "example": 4
          },
          "name": {
            "type": "string",
            "description": "Name of the Article.",
            "example": "Pencil"
          },
          "unitPrice": {
            "type": "number",
            "description": "Unit price of the Article.",
            "example": 1050.25
          },
          "totalPrice": {
            "type": "number",
            "description": "Total price of the Article for required quantity.",
            "example": 4201
          }
        },
        "description": "List of the Items in the Cart."
      },
      "Pageable": {
        "type": "object",
        "properties": {
          "page": {
            "minimum": 0,
            "type": "integer",
            "format": "int32"
          },
          "size": {
            "maximum": 2000,
            "minimum": 1,
            "type": "integer",
            "format": "int32"
          },
          "sort": {
            "type": "array",
            "items": {
              "type": "string"
            }
          }
        }
      }
    }
  }
}